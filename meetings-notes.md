# Moonshot Collective meetings notes

Please let me know if you find these useful, or not, any feedback is good, I'm making these notes for you :)

## Wednesday Noon EST Sync Calls

### 2021-09-01
Our fourth Sync-Up call!

Our tips app with many names graduated to mainnet last week and distributed GTC to MC builders, then more GTC to MC builders Monday, then to MMM Memelords at Memepalooza on Tuesday, and today was used by Kevin and Austin and became tip.party !

Quadratic Diplomacy also saw a mainnet successful distribution of GTC at Memepalooza. This test surfaced a number of issues for us to address in building towards 1.0, and was a success both operationally and as an intelligence gathering experiment. It will be living for a while at quadraticdiplomacy.com 

Anon Vs Moloch is a new MC project just getting started, but with great excitement!

Progress continues on DAOmart and the Steward Report Cards at our MMM-MC detachment, and they have their first tests coming up this Friday. 

After the smashing success of Memepalooza, MMM and MC will be running monthly meme events, and building some tools around that, one tentatively named 'memeseum'.





### 2021-08-25
Our third Sync-Up call, and our kickoff for daily dogfooding cycles for MC Season 0 (S0) projects Quadratic Diplomacy or Q-DIP and Participant Tipping or Q-TIPs.

Our different parallel Q-DIP experiments and teams are combining efforts together to arrive at a single mainnet release candidate, and commits are streaming in. 

We've added as many people and resources as possible at this time to these two projects - we don't want to overdo it, because you know what they say about overprovisioned developers - "What one developer can do in one month, two developers can do in two months"

We talked about finding staff to support and maintain scaffold-eth, particularly the desire for a content and curriculum creator and manager. Genius technical educators, please reach out!

We discussed the state of sign-in-with-eth, which already has a scaffold-eth / buidl guidl implementation, while the ENS RFP process for same is still ongoing. We will be following the lead of ENS on this, actively. MC ran an ENS clinic just before the call and got .eth names for our individual members, which we're using with all our projects. We expect this to be a semi-regular clinic event. 

We made the choice to standardize on Rinkeby as our S0 testnet to reduce friction.

We talked about how to structure information and announcements for MC and GDAO - we're working with MMM to set up a content copyediting team for the DAO that MC will have access to. There was some friction and frustration around the coordination failure to publicly communicate timeline decisions, and a commitment to publish a calendar for S0 to make things more clear and consistent. 

Until the copyediting team is in place, you'll keep getting these typed up freeform by me. :)
Please let our coordinator ryan@ryanpetroff.ca know if you have any feedback about these announcements!






### 2021-08-18
Our second Sync-Up call, and well attended!

Quadratic Diplomacy and Participant-Tipping builds were presented by working teams and helped along by the Collective. We prioritized features and deployment paths so we can show off demos at the upcoming Monday big-public-community call, where each build will have ~5 minutes of the hour. A deployment session will solidify details before the community call.

We talked about the release plan for QD, specifically discussing the 'escrow' feature that will be intentionally left off of early demo builds so team funds aren't lost to bugs or accidents. To have confidence to release a tool with a serious financial feature like escrow, we'll need contract audits - we discussed options and put forward the idea of building a GitcoinDAO Audit Workstream in the months to come.

We talked about how projects should ideally be as web3 as possible, including IPFS frontends. This will be a Collective priority in future cycles.

Telegram and Discord channels for Moonshot Collective are now bridged, unifying our main discussion channels.

Collective projects have expenses, like server costs, which are intentionally 'decentralized' and settled by individual members. To make this fair, moonshotcoordinator.eth has been set up to hold GTC to reimburse individuals, as well as other minor expenses like helping new members onboard with ENS. 

Projects were added to the backlog and future ideas were explored, and for extra points some of our participants attended through AR lenses. Some were pickles, others anime, and in one instance, a Shrek. We work hard, but we have fun. 


### 2021-08-11
Our first sync call! Went well, still obviously early days.
 - For Quadratic Diplomacy:

We paired developers together to bolster strengths, pairing (Hans&Swapp) and (Carlos&Bliss) and setting up a hack session for (Bliss&Varun) with Austin, as well as a session for Carlos and Austin. We expect to hear back on how this went next week, and we'll likely do more pairing and grouping.

Carlos and Hans both gave a live code tour, things are coming along great. 


 - For Moonshot Collective:
 
Our current source of truth about projects is this Trello https://trello.com/invite/b/qDaciNgF/a5baf02df6aeb4260038a8d616d007bf/moonshot-collective (must sign in) (Kevin is admin)

Grooming projects and moving them along requires a simple spec for each project, likely outlining inputs and outputs and lofi sketches. Examples of lightweight spec templates are welcome to help us make grooming and building smoother.

Ryan is trying to collect official MC info and resources here: https://gitlab.com/petroff.ryan/moonshot-collective/-/blob/main/resources.md Please help add anything missed, this definitely is not complete. This is useful because most people on the call didn't know about "GitcoinDAO Egregore is Emerging" Kevin's forum post ( https://gov.gitcoin.co/t/the-gitcoin-gitcoindao-egregore-is-emerging/8200 ) which we should all read. 

Ryan and Austin will work on the pipeline for onboarding new devs, and prioritize keeping devs in stream and not losing them. Part of this will be making sure everyone feels part of a team that's cooperating instead of competing, and making sure our incentives stay aligned. 

A "tips for attendance"/"GTC for call participants" project idea was put forward, and will be an MC priority project alongside QD for our first batch of release products. Next steps are probably hashing out a spec, and Austin might run a session on this ( not yet scheduled, may wait until next week ).

Lastly, MC will run a weekly Dogfooding session on Fridays to demo what gets built each week and coordinate priority feature builds. The first Dogfooding session will be 2PM ET Friday at our clubhouse (jitsi/moonshotcollective) and will be loose and informal and allowed to run long instead of being a controlled meeting.

