# Moonshot Collective Coordination

Hello crypronauts!

This repo exists to help us coordinate the Moonshot Collective - [moonshotcollective.space](https://moonshotcollective.space/)

If you're new to the Collective, welcome! 
Please get in touch with our Ryan ( Moonshot Collective Coordinator ) by booking an onboarding call here: calendly.com/ryanpetroff/discovery-call or email ryan @ ryanpetroff.ca . Please reach out for any reason :)



### Your first mission:

We are now collecting ideas for projects for Moonshot Collective, including inward-facing tools that will make our lives easier as we build the future, as well as big public infrastructure and other useful programs. 

Please add one new idea to this repo as an 'issue' to start. 

( You'll need to be logged in to your GitLab account. Gitcoin today uses GitHub infrastructure, but Moonshot Collective is building a strong foundation of fully Free as in Freedom tools and infrastructure. One of the hardest parts of a moonshot is escaping a gravity well! )   

### Your second mission:
Get a copy of scaffold-eth running on your local development environment: github.com/austintgriffith/scaffold-eth

Pretty cool, right?
Join the scaffold-eth support channel if you run into trouble.

### Your third mission:
Read the 'Ethereum Speed Run' linked by scaffold-eth and look up all the words you don't understand. 🔭🛰


More missions will follow as we evolve! 

#### Coordination Calls
Once you know all the vocabulary, you're ready to join the conversation!
Our weekly collective sync calls happen Wednesdays at 12 Noon EST at meet.jit.si/moonshotcollective
If there's anything you'd like to put on the agenda, please let us know!

We also run Dogfooding Fridays to demo everything build each week, 2pm EST on jitsi. 
Please come show off what you've built and demo what everyone else has been working on!

#### Next steps
Once you're up to speed, onboarded, and integrated, it's time to get to work! 
We'll work together to place you on a team where you'll be able to succeed and grow. 
Make sure your coordinator knows who you are!


