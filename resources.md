# Moonshot Collective info

This is my attempt to collect all the public info about Moonshot Collective.
Please help me add anything I missed or anything new :)

First, the official site: https://moonshotcollective.space/

Announcement for workstream: https://gov.gitcoin.co/t/workstream-dapp-prototyping-the-moonshot-collective/130

Prototyping Workstream (rocket Moonshot) Budget Request: https://gov.gitcoin.co/t/prototyping-workstream-moonshot-budget-request/8074

Announcement at Ethcc 2021: https://www.youtube.com/watch?v=KpXPym_m_wA&t=644s

Trello board of ideas: https://trello.com/invite/b/qDaciNgF/a5baf02df6aeb4260038a8d616d007bf/moonshot-collective (must sign in)

Discord: https://discord.gg/HbHBBpaMds ( Gitcoin discord, moonshot channel not very active )

Telegram: https://t.me/joinchat/Qt4WsU6Rrjk5ODlh

"GitcoinDAO Egregore is Emerging" Kevin's forum post: https://gov.gitcoin.co/t/the-gitcoin-gitcoindao-egregore-is-emerging/8200

Slaying Moloch | Bankless 33 | Ameen Soleimani & Kevin Owocki: https://www.youtube.com/watch?v=903tHM4RA9k

Call 0x0: https://www.youtube.com/watch?v=WQERC6cVhJU
